# Example ASDF Project

[Based on the ASDF manual.](https://www.common-lisp.net/project/asdf/asdf.html)

In learning how to setup a simple common lisp project, I had troubles finding a
well documented example on how to setup a basic project.

Hopefully, this simple example will help others who were confused as I was.
Simply browse the different files to see (hopefully) fully documented
examples of each part.

# Basic Overview

To start with, I would suggest cloning the repo into "~/common-lisp/" to 
allow yourself to browse in your own editor/IDE. 

The entry point into building the project is [example-lisp.asd](example-lisp.asd).
There the system's metadata and build steps are defined via the `defsystem`
macro.


From there, [packages.lisp](packages.lisp) is the first file loaded, which
creates the common-lisp package and specifies/loads the dependencies into it.
For a basic tutorial on packages, see [here](http://www.flownet.com/ron/packages.pdf).

From there, the rest of the build is mostly up to you. `defsystem` is highly
configurable. As of right now, I'd highly suggest looking at the doc.

# Building

In a terminal, start up your preferred common lisp repl (ie sbcl).
Try to load the program `example-lisp`, which is the name of our system
in [example-lisp.asd](example-lisp.asd).


```common-lisp
(asdf:load-system "example-lisp")

;; debugger invoked on a ASDF/FIND-COMPONENT:MISSING-DEPENDENCY in thread
;; #<THREAD "main thread" RUNNING {100399C4B3}>:
;;   Component "optima.ppcre" not found, required by #<SYSTEM "example-lisp">
;;
;; Type HELP for debugger help, or (SB-EXT:EXIT) to exit from SBCL.
```

Whoops. This is because `optima.ppcre` is required in our system defintion
(see the :depends-on section). Unfortunately, ASDF is simply for defining
our build, not for pulling down libraries we depend on and installing them
as well.

For this, we turn to [Quicklisp](https://www.quicklisp.org). Long story
short, quicklisp is a library management tool that will download and install
libraries we want to use.

There are other ways of handling this, but currently quicklisp is the standard.
Luckily, it's extremely easy to setup. I suggest following the instructions on
the site to get everything setup. I particularly suggest you look at 
`(ql:add-to-init-file)`, which will load quicklisp when you start up your
common-lisp implementation.

So, once you've installed quicklisp, if we open up a new repl...

```common-lisp
(ql:quickload "optima.ppcre")
;; ... ("optima.ppcre")

(ql:quickload "command-line-arguments")
;; ... ("command-line-arguments")

(asdf:load-system "example-lisp")
;; ...
; compilation finished in 0:00:00.000
T
```

Tada! We've successfully built the project.

# Contributing

Feel free to send any pull requests with additional
examples and or better/more-full explanations.