;;;; packages.lisp: Our first lisp source file for our project.

;; Make sure we are in the default common-lisp package.
;; This ensures that we have a default environment
;; with which to work with and everything we are
;; expecting to be defined is so.
;; For more information on default packages, visit
;; https://www.cs.cmu.edu/Groups/AI/html/cltl/clm/node117.html
(in-package :cl)

;; Create the package for our project.
(defpackage example-lisp

  ;; In our new package, load these dependencies.
  (:use

   ;; ':cl' is the common-lisp package containing
   ;; all of our default symbols most programs might
   ;; require.
   ;; www.lispworks.com/documentation/HyperSpec/Body/01_i.htm
   :cl

   ;; Our libraries we defined in example.asd
   ;; that we were going to use.
   :optima.ppcre
   :command-line-arguments
   ;; Careful, this can pollute your namespace.
   ;; You might just want to use :import-from
   ;; or simply refer to the fully qualified
   ;; packages exported symbols (ie. "package:my-func")
   )

  ;; We can also import individual symbols from a given
  ;; package to help prevent polluting our namespace.
  (:import-from :cl-fad #:file-exists-p)

  ;; Finally, The list of symbols we're going to export
  ;; from our package. Our public API.
  (:export #:my-test
           ;; ...
           ))


;; Now we switch to our newly created package
;; so that all following definitions are defined
;; in our new package.
(in-package :example-lisp)
