;;;; From https://www.common-lisp.net/project/asdf/asdf.html#Defining-systems-with-defsystem

;; A 'system' is a complete project, whether that
;; be a library or an application.

;; The '.asd' file is what asdf searches for in order
;; to find systems to load.

;; asdf searches through multiple default folders in
;; search of systems that are available
;; locally. One of these folders is "~/common-lisp/*".
;; These folders are configurable, see
;; https://www.common-lisp.net/project/asdf/asdf.html#Configuring-ASDF
;; for more info.


;; ASDF recommends that the system definition should be the ONLY form
;; in the file, for future compatability and simplicity.
(defsystem "example-lisp"
  ;;; Associated metadata with this system.

  ;; :description, :version, :author, and :licence should
  ;; be provided, especially if you want your project to
  ;; eventually be included with quicklisp (library manager,
  ;; https://www.quicklisp.org/)
  :description "example-lisp: a sample Lisp system."
  :version "0.0.1" ;; can only be period-separated non-negative integers
  :author "Joe User <joe@example.com>"
  :licence "Public Domain"

  ;;; List of dependencies for this system.

  ;; Modules we require for our project.
  ;; TODO: explain how these are loaded
  :depends-on ("optima.ppcre" "command-line-arguments" "cl-fad")

  ;; Finally. Our project's actual files and code,
  ;; and how asdf is supposed to load and build
  ;; our system.
  :components ((:file "packages")
               (:file "macros" :depends-on ("packages"))
               (:file "example" :depends-on ("macros"))

               ;; Modules allow you to create sub-directories
               ;; in your project. ie, "/src", "/test", etc...
               ;; and accept basically the same parameters that
               ;; the defsystem macro takes.
               (:module "test" ;; name of the module, and by default the dir name

                        :depends-on ("example")

                        ;; Have these files load serially, such that we
                        ;; say each subsequent file depends on the previous.
                        ;; We could also define this at the top level
                        ;; if we wanted to.
                        :serial t

                        ;; pathname keyword allows you to specify
                        ;; that this module lives in a path different
                        ;; than the name of the module

                        ;; :pathname "my-path"

                        :components
                        ((:file "example-test")))))



;; A system definition can be much more compilcated than this,
;; this example is just supposed to show off the basics.
;; Look at the asdf documentation for more in-depth examples
;; and explanations.
